![Proxmox](/attachments/mediakit-proxmox-server-solutions-logos-dark.svg)

# **Proxmox geting started**

#### Post Install Configuration

H2DC - How to do Computers [https://www.youtube.com/watch?v=R0Zn0bdPwcw&t](https://www.youtube.com/watch?v=R0Zn0bdPwcw&t)

#### Proxmox Helper Scripts

TTeck \* (A must use) [https://tteck.github.io/Proxmox/](https://tteck.github.io/Proxmox/)

#### Docker Setup

An easy way to setup docker and portainer [pi-hosted](https://github.com/pi-hosted/pi-hosted) this is Portainer Based, along with YT tutorials.

[Dockge](https://github.com/louislam/dockge) An easy way to install via docker compose

Links to Proxmox tutorials :-

* https://www.youtube.com/@ElectronicsWizardry/videos
* https://www.youtube.com/@virtualizeeverything/videos
* https://www.youtube.com/@Practical-IT/videos
* https://www.youtube.com/@MRPtech/videos
* https://www.youtube.com/@Jims-Garage/videos

Links to docker tutorials :-

* https://www.youtube.com/@Techdox /video
* https://www.youtube.com/@AwesomeOpenSource/videos
* https://www.youtube.com/@DBTechYT/videos
* https://www.youtube.com/@bigbeartechworld/videos

Obviously there are lots of others available these are only a sample - ones that I have used.
